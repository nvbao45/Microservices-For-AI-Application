from pydantic import BaseModel, EmailStr
from fastapi import UploadFile
from datetime import datetime
from typing import List


class Chart(BaseModel):
    file_url: str = 'upload://ZVQGW1N6B4'
    use_col: List[int] = [1]
    start: int = 0
    end: int = -1
