import axios from 'axios';
import { message } from 'antd';
import { AxiosError } from 'axios';
import { ApiError } from '@app/api/ApiError';
import { readToken } from '@app/services/localStorage.service';



export const httpApi = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  timeout: 5000,
});

httpApi.interceptors.request.use((config) => {
  config.headers = { ...config.headers, Authorization: `Bearer ${readToken()}` };
  return config;
});

httpApi.interceptors.response.use(
  (response) => {
    return response;
  },
  (error: AxiosError) => {
    if (error.response?.status === 401) {
      window.location.href = '/auth/login';
    }
    message.error(error.response?.data.detail);
    throw new ApiError<ApiErrorData>(
      error.response?.data?.detail || error.response?.data?.message || error.message,
      error.response?.data,
    );
  },
);

export interface ApiErrorData {
  detail?: string;
}
