import numpy as np
import pandas
import tensorflow as tf
import pika
import os

from app.core.config import settings
from app.service.task.task import TaskService

from celery import Celery
from celery.utils.log import get_task_logger
from celery.signals import worker_init, worker_process_init
from celery.concurrency import asynpool

from tensorflow import keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import LSTM
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error


asynpool.PROC_ALIVE_TIMEOUT = 100.0 #set this long enough
logger = get_task_logger(__name__)
lstm_celery = Celery(
    'lstm', 
    broker=f'pyamqp://{settings.RABBITMQ_USER}:{settings.RABBITMQ_PASS}@{settings.RABBITMQ_HOST}//',
    backend=f'rpc://{settings.RABBITMQ_USER}:{settings.RABBITMQ_PASS}@{settings.RABBITMQ_PASS}//'
)
lstm_celery.conf.task_serializer   = 'json'
lstm_celery.conf.result_serializer = 'json'
rabbitmq_connection = pika.BlockingConnection(
                    pika.ConnectionParameters(
                        settings.RABBITMQ_HOST,
                        credentials=pika.PlainCredentials(settings.RABBITMQ_USER, settings.RABBITMQ_PASS)
                    ))


class CustomCallback(keras.callbacks.Callback):
    def __init__(self, user, task_id, token, name, description):
        super(CustomCallback, self).__init__()
        self.user = user
        self.token = token
        self.name = name
        self.description = description
        self.task_service = TaskService(settings.TASK_SERVICE, token)
        self.task_id = task_id;

        self.routing_key = f"{user}/lstm_logs"
        self.batch_routing_key = f"{self.routing_key}/batch"
        self.epoch_routing_key = f"{self.routing_key}/epoch"
        self.channel = rabbitmq_connection.channel()
        self.channel.queue_declare(queue=self.batch_routing_key)
        self.channel.queue_declare(queue=self.epoch_routing_key)

        queue = self.batch_routing_key + "|" + self.epoch_routing_key
        self.task_service.update_task(self.task_id, { 'status': 'Training', 'ramq_queue': queue})

    def on_train_batch_end(self, batch, logs=None):
        output = "Up to batch {}, the average loss is {}.".format(batch, logs["loss"])
        self.channel.basic_publish(exchange='',
                            routing_key=self.batch_routing_key,
                            body=output)

    def on_epoch_end(self, epoch, logs=None):
        output = "The average loss for epoch {} is {}".format(epoch, logs["loss"])
        self.channel.basic_publish(exchange='',
                            routing_key=self.epoch_routing_key,
                            body=output)

    def on_train_end(self, logs=None):
        self.task_service.update_status(self.task_id, "Train end")
        try:
            os.mkdir(f'models/{self.user}')
        except:
            pass
        
        model_name = f'{self.user}_{self.name}_{self.task_id}_LSTM-Model.h5'
        self.model.save(f"models/{self.user}/{model_name}", save_format='h5')
        self.task_service.update_task(self.task_id, {'output': model_name})


def create_dataset(dataset, look_back=1):
	dataX, dataY = [], []
	for i in range(len(dataset)-look_back-1):
		a = dataset[i:(i+look_back), 0]
		dataX.append(a)
		dataY.append(dataset[i + look_back, 0])
	return np.array(dataX), np.array(dataY)


@lstm_celery.task
def train(user, task_id, token, name, description, file_url, look_back, split, loss_fun, optimizer, epochs, batch_size):  
    task_service = TaskService(settings.TASK_SERVICE, token) 
    try:
        dataframe = pandas.read_csv(file_url, usecols=[1], engine='python')
        dataset = dataframe.values
        dataset = dataset.astype('float32')
        scaler = MinMaxScaler(feature_range=(0, 1))
        dataset = scaler.fit_transform(dataset)
        train_size = int(len(dataset) * split)
        test_size = len(dataset) - train_size
        train, test = dataset[0:train_size,:], dataset[train_size:len(dataset),:]
        trainX, trainY = create_dataset(train, look_back)
        testX, testY = create_dataset(test, look_back)
        trainX = np.reshape(trainX, (trainX.shape[0], 1, trainX.shape[1]))
        testX = np.reshape(testX, (testX.shape[0], 1, testX.shape[1]))
        model = Sequential()
        model.add(LSTM(4, input_shape=(1, look_back)))
        model.add(Dense(1))
        model.compile(loss=loss_fun, optimizer=optimizer)
        model.fit(
            trainX, 
            trainY, 
            epochs=epochs, 
            batch_size=batch_size, 
            verbose=0, 
            callbacks=[CustomCallback(user, task_id, token, name, description)]
        )
    except Exception as e:
        task_service.update_task(task_id, { 'status': 'Error', 'output': str(e)})
    return 'Done'


if __name__ == '__main__':
    lstm_celery.worker_main(argv=['worker', '--loglevel=info'])