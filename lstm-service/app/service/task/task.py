import os
import httpx


class TaskService:
    def __init__(self, host, token) -> None:
        self.token = token
        self.host = host
        self.headers = {
            "Authorization": f"Bearer {self.token}"
        }

    def task(self, name, description, queue, status, service):
        return {
            "name": name,
            "description": description,
            "ramq_queue": queue,
            "service": service,
            "status": status
        }

    def create_task(self, task):
        r = httpx.post(f"{self.host}/create-task", headers=self.headers, json=task)
        return r
    
    def update_status(self, id, status):
        data = {
            "status": status
        }
        r = httpx.post(f"{self.host}/change-status/{id}", headers=self.headers, json=data)
        return r

    def update_task(self, id, task):
        r = httpx.post(f"{self.host}/update/{id}", headers=self.headers, json=task)
        return r
