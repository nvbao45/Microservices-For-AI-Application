import React, { useEffect, useState, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { notificationController } from '@app/controllers/notificationController';
import { useSearchParams } from 'react-router-dom';
import useWebSocket, { ReadyState } from 'react-use-websocket';
import { Divider, List, Typography } from 'antd';
import { readToken } from '@app/services/localStorage.service';

import * as S from '@app/components/tables/Tables/Tables.styles';
import { PageTitle } from '@app/components/common/PageTitle/PageTitle';
const RabbitMQPage: React.FC = () => {
  const { t } = useTranslation();
  const [searchParams, setSearchParams] = useSearchParams();
  const [socketUrl, setSocketUrl] = useState(
    `ws://localhost:8001/api/v1/task/queue?queue=${searchParams.get('queue')}`,
  );
  const [messageHistory, setMessageHistory] = useState([]);
  const { sendMessage, lastMessage, readyState } = useWebSocket(socketUrl);

  const evtSource = new EventSource(`http://localhost/api/v1/lstm/logs?token=${readToken()}`);

  const connectionStatus = {
    [ReadyState.CONNECTING]: 'Connecting',
    [ReadyState.OPEN]: 'Open',
    [ReadyState.CLOSING]: 'Closing',
    [ReadyState.CLOSED]: 'Closed',
    [ReadyState.UNINSTANTIATED]: 'Uninstantiated',
  }[readyState];

  useEffect(() => {
    evtSource.addEventListener('new_message', function (event) {
      console.log(event.data);
    });
    evtSource.addEventListener('end_event', function (event) {
      console.log(event.data);
      evtSource.close();
    });

    return () => {
      evtSource.close();
    };
  }, []);
  // useEffect(() => {
  //   if (lastMessage !== null) {
  //     setMessageHistory((prev) => {
  //       // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  //       // @ts-ignore
  //       return prev.concat(lastMessage);
  //     });
  //   }
  // }, [lastMessage, setMessageHistory]);

  return (
    <>
      <PageTitle>LSTM</PageTitle>
      <S.TablesWrapper>
        <S.Card key="rabbitmq-logs" title="Rabbit MQ">
          <List
            size="small"
            header={<span>Message: {lastMessage?.data}</span>}
            bordered
            dataSource={messageHistory}
            renderItem={(item) => (
              <List.Item>
                <Typography.Text>[+] </Typography.Text>
                {item['data']}
              </List.Item>
            )}
          />
        </S.Card>
      </S.TablesWrapper>
    </>
  );
};
export default RabbitMQPage;
