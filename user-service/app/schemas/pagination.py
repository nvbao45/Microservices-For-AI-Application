from pydantic import BaseModel, EmailStr
from typing import Any, List


class Pagination(BaseModel):
    current: int
    pageSize: int

class PaginationShow(BaseModel):
    current: int
    pageSize: int
    total: int
    class Config: # to convert non dict obj to json
        orm_mode = True