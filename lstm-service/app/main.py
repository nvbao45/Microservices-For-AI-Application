from fastapi import FastAPI
from app.api.v1.lstm import lstm_router

# from app.db.session import engine, database
# from app.db.base_class import Base
from app.core.config import settings
from app.service.celery.lstm import lstm_celery
from fastapi.middleware.cors import CORSMiddleware

from pyctuator.pyctuator import Pyctuator
from pyctuator.health.db_health_provider import DbHealthProvider

from multiprocessing import Process

# Base.metadata.create_all(bind=engine)


app = FastAPI(
    openapi_url=f"/api/{settings.VERSION}/lstm/openapi.json",
    docs_url=f"/api/{settings.VERSION}/lstm/docs"
)

origins = [
    "http://localhost",
    "http://localhost:3000",
    "http://localhost:8888"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"]
)

def start_scheduler():
    lstm_celery.worker_main(argv=['worker', '--loglevel=info'])
celery_process = Process(target=start_scheduler)

@app.on_event("startup")
async def startup():
    celery_process.start()

@app.on_event("shutdown")
async def shutdown():
    celery_process.terminate()

app.include_router(lstm_router, prefix=f"/api/{settings.VERSION}/lstm", tags=["Long short term memory"])


# pyctuator = Pyctuator(
#     app,
#     "LSTM Service",
#     app_url="http://lstm-service:8000",
#     pyctuator_endpoint_url="http://lstm-service:8000/pyctuator",
#     registration_url="http://spring-admin:8080/instances",
#     app_description="Long short term memory service",
#     additional_app_info=dict(
#         serviceLinks=dict(
#             API_Docs="http://localhost/api/v1/lstm/docs"
#         ),
#     )
# )
# pyctuator.set_build_info(
#     name="LSTM Service",
#     version="1.0"
# )


# pyctuator.register_health_provider(DbHealthProvider(engine))