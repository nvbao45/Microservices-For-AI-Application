from fastapi import APIRouter, HTTPException, status
from fastapi import WebSocket, Request, Header, BackgroundTasks
from fastapi.responses import HTMLResponse

from fastapi import Depends
from fastapi.responses import StreamingResponse
from sqlalchemy.orm import Session
from typing import List, Union
from fastapi.responses import FileResponse, StreamingResponse

# from app.db.session import get_db
from app.deps.auth import get_current_user_from_token
from app.schemas.chart import Chart
from app.schemas.training import Training
from app.core.config import settings


from sse_starlette.sse import EventSourceResponse


# app service
from app.service.rabbitmq.rabbitmq import Notifier
from app.service.celery.lstm import train
from app.service.task.task import TaskService

from urllib.error import HTTPError
from starlette.websockets import WebSocket, WebSocketDisconnect

import io
import os
import matplotlib.pyplot as plt
import pandas


lstm_router = APIRouter()


async def process_file_url(file_url: str, authorization: str):
    if "http://" in file_url or "https://" in file_url:
        file_url = file_url
        return file_url
    if "upload://" in file_url:
        file_id = file_url.replace("upload://", "")
        token = authorization.replace("Bearer ", "")
        file_url = f'{settings.FILE_SERVICE}/{file_id}/download?token={token}'
        return file_url


@lstm_router.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket, id: str):
    notifier = Notifier()
    await notifier.connect(websocket)
    await notifier.setup(f"{id}/lstm_logs/batch", f"tag_{id}")
    try:
        while True:
            data = await websocket.receive_text()
            await websocket.send_text(f"Message text was: {data}")
    except WebSocketDisconnect:
        notifier.remove()


@lstm_router.post("/chart", response_class=StreamingResponse)
async def get_chart_from_csv_file(
    req: Chart,
    user: str = Depends(get_current_user_from_token),
    authorization: Union[str, None] = Header(None)
):
    if req.end < req.start and req.end != -1:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="End record cannot be less than start record!!!!"
        )
    try:
        file_url = await process_file_url(req.file_url, authorization)
        dataset = pandas.read_csv(file_url, usecols=req.use_col, engine='python')
    except HTTPError as e:
        raise HTTPException(
            status_code=e.code,
            detail=str(e)
        )
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=str(e)
        )
    try:
        plt.plot(dataset[req.start:req.end])
        buf = io.BytesIO()
        plt.savefig(buf, format='png')
        plt.clf()
        buf.seek(0)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(e)
        )
    return StreamingResponse(buf, media_type='image/png')


import random
import asyncio

@lstm_router.get("/logs")
async def logs_stream(
    token: str,
    req: Request,
):
    user = get_current_user_from_token(token)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Not authenticated"
        )
    async def event_generator():
        while True:
            if await req.is_disconnected():
                break

            yield {
                "event": "new_message",
                "id": "sdfsdf",
                "retry": 15000,
                "data": random.randint(0, 100)
            }
            await asyncio.sleep(1)
    return EventSourceResponse(event_generator());


@lstm_router.post("/train")
async def training(
    req: Training,
    user: str = Depends(get_current_user_from_token),
    authorization: Union[str, None] = Header(None)
):
    file_url = await process_file_url(req.file_url, authorization)
    token = authorization.replace("Bearer ", "")

    task = TaskService(settings.TASK_SERVICE, token)
    response = task.create_task(
        task.task(req.name, req.description, queue='', status='Task Created', service='lstm')
    )
    if response.status_code == 200:
        try:
            print(response.json())
            task_id = response.json()['id']
            train.delay(
                user['username'],
                task_id,
                token, 
                req.name,
                req.description,
                file_url,
                req.look_back,
                req.split,
                req.loss_fun,
                req.optimizer,
                req.epochs,
                req.batch_size
            )
            return {"message": "Job sent, training in progress"}
        except:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Error"
            )
    raise HTTPException(
        status_code=response.status_code,
        detail="Error"
    )

@lstm_router.get('/{model}/download', response_class=FileResponse)
def download_file(
    model: str,
    token: str,
): 
    user = get_current_user_from_token(token=token)
    file_path = f'models/{user["username"]}/{model}'

    if os.path.exists(file_path):
        return FileResponse(
            path=f'models/{user["username"]}/{model}',
            filename=model
        )
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="File not found!!!!"
        )