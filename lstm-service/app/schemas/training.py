from pydantic import BaseModel, EmailStr
from fastapi import UploadFile
from datetime import datetime
from typing import List


class Training(BaseModel):
    name: str = "LSTM __"
    description: str = ""
    file_url: str = 'upload://ZVQGW1N6B4'
    look_back: int = 1
    split: float = 0.7
    loss_fun: str = 'mean_squared_error'
    optimizer: str = 'adam'
    epochs: int = 2
    batch_size: int = 1